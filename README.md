<h1 align="center"><a href="https://github.com/java-aodeng" target="_blank">Hope-Boot</a></h1>

<p align="center">
<a href="http://ilovey.live"><img alt="Author" src="https://img.shields.io/badge/author-%E4%BD%8E%E8%B0%83%E5%B0%8F%E7%86%8A%E7%8C%AB-blue.svg"/></a>
<a href="https://github.com/hope-for/hope-boot"><img alt="JDK" src="https://img.shields.io/badge/JDK-1.8-orange.svg"/></a>
<a href="https://github.com/hope-for/hope-boot/blob/master/LICENSE"><img alt="license" src="https://img.shields.io/github/license/java-aodeng/hope.svg?style=flat-square"/></a>
<a href="https://jq.qq.com/?_wv=1027&k=574chhz"><img alt="QQ群" src="https://img.shields.io/badge/chat-%E4%BD%8E%E8%B0%83%E5%B0%8F%E7%86%8A%E7%8C%ABQQ%E7%BE%A4-yellow.svg"/></a>
<a href="https://t.me/joinchat/LSsyBxVKLGEkF5MtIhg6TQ"><img alt="Telegram" src="https://img.shields.io/badge/telegram-%E4%BD%8E%E8%B0%83%E5%B0%8F%E7%86%8A%E7%8C%AB--%E5%AE%98%E6%96%B9%E9%83%A8%E8%90%BD-orange.svg"/></a>
<a href="https://github.com/hope-for/hope-boot"><img alt="star" src="https://img.shields.io/github/stars/hope-for/hope-boot.svg?label=Stars&style=social"/></a>
<a href="https://github.com/hope-for/hope-boot/releases"><img alt="发行版本" src="https://img.shields.io/badge/release-%E5%8F%91%E8%A1%8C%E7%89%88%E6%9C%AC-red.svg"/></a>
</p>

## 简介:

> 一款现代化的脚手架项目。企业开发？接外包？赚外快？还是学习？这都能满足你，居家必备，值得拥有:beers:
------------------------------

## 文档:

使用说明：[https://github.com/hope-for/hope-boot/wiki](https://github.com/hope-for/hope-boot/wiki)

## 捐赠:

>请作者吃个肉夹馍 :)

![](https://i.loli.net/2018/12/31/5c29d3b18826d.png)

## Links:
- [CI/CD 流程以及原理说明](https://gitbook.cn/gitchat/activity/5daac87dd39d6a72d183b52c?utm_source=weixinNotification)
- [Github](https://github.com/java-aodeng)
- [熊猫哥星球基地](https://t.zsxq.com/rn2jUbY)
- [学习-《panda全家桶》-只要￥9.98-打包回家](https://mianbaoduo.com/o/bread/mbd-aZeVlw== )
